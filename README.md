# course-scheduling

恭喜可以在线预览， 感兴趣的同学可以联系作者   访问密码 联系作者  qq  1462638689

 **预览地址 [ http://www.aliwo.cn/#/](http://)** 


#### 介绍
先给个Star再看嘛！

基于遗传算法的高中排课系统。 该项目是针对高校的教学生态，本人将其全部改版成了适用于高中阶段的教学生态， 当然了，节次数跟当前的高中肯定是不一样的，因为我当时的课题是为培训机构安排， 大家只需要更改时间片的数量即可实现不同节次课程的编排，照葫芦画瓢即可。

#### 软件架构
软件架构说明
前端技术栈： Vue2.x + Element UI，使用npm包管理工具

后端技术栈： JDK1.8 + Spring Boot + MySQL8.0 + Mybatis-Plus，使用maven实现包管理，开发工具：IDEA

#### 安装教程

配置好JDK环境，node.js环境，安装好Vue脚手架工具以及maven环境
前端项目在UI目录下的文件夹内，在CourseArrange目录下运行命令：npm install 安装完相关的前端依赖
执行 npm run dev 命令将前端项目启动
后端项目在配置好JDK环境之后，使用IDEA等IDE工具打开，将项目配置成自己本地的Maven(建议使用阿里云镜像)
运行maven安装项目所需依赖，配置好数据库的连接，待依赖安装完成启动项目的Application启动类即可(不会的也不教了哈)
用到文件上传功能时，需要配置好自己的阿里云key与密钥，在utils下面的AliyunUtil类下面

实现功能
    系统有管理员（教务处主任）、讲师、学生三种用户

    前端比较菜，应用启动后进入的引导页面如下，根据需求进入不同登录页面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214229_db11b2ae_7927376.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214303_7947a920_7927376.png "屏幕截图.png")
这里主要放管理员的功能截图 1）管理员登录成功后进入到系统数据页面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214438_a8102b17_7927376.png "屏幕截图.png")
网卡管理：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214842_61b495f4_7927376.png "屏幕截图.png")
排课管理：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214459_4a5f7b60_7927376.png "屏幕截图.png")
查看课表：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214535_64a33963_7927376.png "屏幕截图.png")

课程管理：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214606_23fba242_7927376.png "屏幕截图.png")
班级管理：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214625_b9a9b6ea_7927376.png "屏幕截图.png")

学生管理：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214641_a9c5083f_7927376.png "屏幕截图.png")
教学资料管理：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/214701_7e0a6910_7927376.png "屏幕截图.png")
