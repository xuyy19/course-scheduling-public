package com.aliwo.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.ObjectMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author xuyayuan
 * @data 2021年04月04日
 */
@Component
public class AliyunUtil {
    private static final Logger LOG = LoggerFactory.getLogger(AliyunUtil.class);

    private static String endpoint = "xx";
    private static String accessKeyId = "xx";
    private static String accessKeySecret = "xx";
    private static String bucketName = "xx";


    /**
     * 文件上传成功返回路径
     *
     * @param file
     * @param directory 选择需要上传到的目录下面，暂时不用，原本想将用户头像、其他文件上传到不同的目录下面的
     * @return
     */
    public static Map<String, Object> upload(MultipartFile file, String directory) {

        String url = "";
        try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            // 获取输入流
            InputStream inputStream = file.getInputStream();
            String fileName = file.getOriginalFilename();
            //1 在文件名称里面添加随机唯一的值
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            // 随机id
            String newFileName = uuid + fileName;
            // 添加 ContentType
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType(getcontentType(fileName.substring(fileName.lastIndexOf("."))));
            ossClient.putObject(bucketName, fileName, inputStream, objectMetadata);
            ossClient.shutdown();
            url = "https://" + bucketName + "." + endpoint + "/" + fileName;
            LOG.info("url========" + url);
            Map<String, Object> map = new HashMap<>();
            map.put("url", url);
            map.put("name", fileName);
            return map;
        } catch (IOException e) {
            LOG.error(String.valueOf(e));
        }
        return null;
    }

    /**
     * 文件下载到本地
     */
    public static String download(String fileName) {
        LOG.info("1" + fileName);
        LOG.info("阿里云开始下载文件到本地");
        String path = "D:\\arrange\\tempfile\\" + fileName;
        LOG.info("阿里云开始下载文件到本地的路径为：" + path);
        OSS ossClient = null;
        try {
            File file = new File(path);
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            ossClient.getObject(new GetObjectRequest(bucketName, fileName), file);
        } catch (Exception e) {
            LOG.error(String.valueOf(e));
        } finally {
            ossClient.shutdown();
        }
        return path;
    }

    /**
     * @param FilenameExtension
     * @return java.lang.String
     * @author jitwxs
     * @date 2021/4/5 22:08
     */
    public static String getcontentType(String FilenameExtension) {
        if (FilenameExtension.equalsIgnoreCase(".bmp")) {
            return "image/bmp";
        }
        if (FilenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (FilenameExtension.equalsIgnoreCase(".jpeg") ||
                FilenameExtension.equalsIgnoreCase(".jpg") ||
                FilenameExtension.equalsIgnoreCase(".png")) {
            return "image/jpg";
        }
        if (FilenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (FilenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (FilenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (FilenameExtension.equalsIgnoreCase(".pptx") ||
                FilenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (FilenameExtension.equalsIgnoreCase(".docx") ||
                FilenameExtension.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (FilenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        return "image/jpg";
    }

}