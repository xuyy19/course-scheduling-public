package com.aliwo.algorithm;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xuyayuan
 * 算法 01 给定⼀个数组和⼀个⽬标和，从数组中找两个数字相加等于⽬标和，输出这两个数字的下标。
 * @data 2021年05月25日
 */
public class DemoOne {
    /**
     * 第一种解法测试
     * 暴力解法 通过双重for循环，时间复杂度 O(n*n)，空间复杂度 O(1)
     *
     * @author xuyayuan
     * @date 2021/5/25 23:41
     */
    @Test
    public void test01() {
        Integer[] integers = new Integer[]{1, 3, 4, 6, 7, 9, 10, 20, 12};
        Integer target = 13;
        Integer[] ans = this.twoSumTarget01(integers, target);
        for (int i = 0; i < ans.length; i++) {
            System.out.println(ans[i]);
        }
    }

    /**
     * @param integers
     * @param target
     * @return java.lang.Integer[]
     * 第一种解法 暴力解法 通过双重for循环，时间复杂度 O(n*n)，空间复杂度 O(1)
     * @author xuyayuan
     * @date 2021/5/25 23:49
     */
    public Integer[] twoSumTarget01(Integer[] integers, Integer target) {
        Integer[] ans = new Integer[2];
        for (int i = 0; i < integers.length; i++) {
            for (int j = (i + 1); j < integers.length; j++) {
                if (integers[i] + integers[j] == target) {
                    ans[0] = i;
                    ans[1] = j;
                }
            }
        }
        return ans;
    }

    /**
     * 第二种解法：通过map进行存储数据，map的key为数据的值，map的value为数组的下标
     *
     * @author jitwxs
     * @date \ 23:55
     */
    @Test
    public void test02() {
        Integer[] integers = new Integer[]{1, 3, 4, 6, 7, 9, 10, 20, 12};
        Integer target = 13;
        Integer[] ans = this.twoSumTarget02(integers, target);
        for (int i = 0; i < ans.length; i++) {
            System.out.println(ans[i]);
        }
    }

    public Integer[] twoSumTarget02(Integer[] integers, Integer target) {
        Map<Integer, Integer> map = new HashMap<>();
        // 把数组的元素通过map进行 存储，key为数组的值，value为数组的下标
        for (int i = 0; i < integers.length; i++) {
            map.put(integers[i], i);
        }
        for (int i = 0; i < integers.length; i++) {
            Integer sub = target - integers[i];
            if (map.containsKey(sub) && map.get(sub) != i) {
                return new Integer[]{i, map.get(sub)};
            }
        }
        throw new IllegalArgumentException("没有找到");
    }


}
