package com.aliwo.stream.lambda;/**
 * package_name:com.aliwo.stream.lambda
 *
 * @author:徐亚远 Date:2021/5/20 12:24
 * 项目名:course-scheduling
 * Description:TODO
 * Version: 1.0
 **/

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xuyayuan
 * @data 2021年05月20日
 */
public class TimeThread extends Thread{
    @Override
    public void run() {
        while (true) {
            // 需要显示出来的时间的格式,SimpleDateFormat类的使用
            SimpleDateFormat s = new SimpleDateFormat("HH:mm:ss");// 参数代表的是时间的显示格式
            System.out.println("当前的时间是：" + s.format(new Date()));// new Date()是当前时间的对象
            //显示时间后睡眠1秒
            try {
                sleep(1000);
            } catch (InterruptedException e) {
               throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        TimeThread timeThread = new TimeThread();
        // 启动线程
        timeThread.start();
    }
}
