package com.aliwo.stream.lambda;/**
 * package_name:com.aliwo.stream.lambda
 *
 * @author:徐亚远 Date:2021/5/17 20:29
 * 项目名:course-scheduling
 * Description:TODO
 * Version: 1.0
 **/

import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author xuyayuan
 * @data 2021年05月17日
 */
public class DemoOne {
        public static void main(String[] args) throws IOException {
            Demo_02();
        }

        private static void Demo_02() {
            System.out.print("请输入一个整数求阶乘:");
            String s = new Scanner(System.in).nextLine();
            System.out.println(Mulptiy(new BigInteger(s)).toString());
        }

        public static BigInteger Mulptiy(BigInteger n) {
            if(n.intValue() == 1) return new BigInteger("1");
            BigInteger sub = new BigInteger("1");
            BigInteger bit = n.subtract(sub);
            return n.multiply(Mulptiy(bit));
        }
}
