package com.aliwo.stream.lambda;

/**
 * @author xuyayuan
 * @data 2021年05月20日
 */
public class DemoTwo {

    public static void main(String[] args) {
        int s = 1;
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                s = i * j;
                //用的是System.out.print()
                System.out.print(i + "*" + j + "=" + s + "\t");
            }
            //起到换行的作用
            System.out.println();
        }
    }

}
