package com.aliwo.stream.lambda;/**
 * package_name:com.aliwo.stream.lambda
 *
 * @author:徐亚远 Date:2021/5/19 14:25
 * 项目名:course-scheduling
 * Description:TODO
 * Version: 1.0
 **/

import java.util.Scanner;

/**
 * @author xuyayuan
 * @data 2021年05月19日
 */
public class SimpleCube {
    //double length; // 长
    //double width; //宽
    //double height; // 高
    //
    //public double getLength() {
    //    return length;
    //}
    //
    //public void setLength(double length) {
    //    this.length = length;
    //}
    //
    //public double getWidth() {
    //    return width;
    //}
    //
    //public void setWidth(double width) {
    //    this.width = width;
    //}
    //
    //public double getHeight() {
    //    return height;
    //}
    //
    //public void setHeight(double height) {
    //    this.height = height;
    //}
    //
    //public SimpleCube() {
    //    length = 0;
    //    width = 0;
    //    height = 0;
    //}
    //
    ///**
    // * 求体积
    // *
    // * @return double
    // * @author xuyy
    // * @date 2021/5/19 14:30
    // */
    //public double volumn() {//求体积
    //    double volumnValue;
    //    volumnValue = length * width * height;
    //    return volumnValue;
    //}

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();


        String str1 = wordsReverse(str);
        //输出为student. a am I
        System.out.println(str1);

    }
    public static String wordsReverse(String str) {
        //字符串变为字符串数组
        String[] strArr = str.split(" ");
        String[] strArr1 = new String[strArr.length];
        StringBuffer sb= new StringBuffer();
        for(int i=0;i<strArr.length;i++){
            //逆序赋值给字符串数组strArr1
            strArr1[i]=strArr[strArr.length-i-1];
            //如果不是最后一个单词则追加该单词和空格，最后一个则只追加该单词
            if(i!=strArr.length-1){
                sb.append(strArr1[i]);
                sb.append(" ");
            }else sb.append(strArr1[i]);

        }


        return sb.toString();
    }

}
