package com.aliwo.stream.lambda;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author xuyayuan
 * @data 2021年05月20日
 */
public class DemoCount {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("D:/test/src/test.txt"));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String line = br.readLine();
            if (line == null)
                break;
            sb.append(line);
        }
        br.close();
        String content = sb.toString();
        // 输出字母的个数
        System.out.println("Character count is:" + (content.length() - content.replaceAll("[a-zA-Z]", "").length()));
    }
}
