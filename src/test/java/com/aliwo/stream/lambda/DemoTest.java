package com.aliwo.stream.lambda;/**
 * package_name:com.aliwo.stream.lambda
 *
 * @author:徐亚远 Date:2021/5/17 20:26
 * 项目名:course-scheduling
 * Description:TODO
 * Version: 1.0
 **/

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xuyayuan
 * @data 2021年05月17日
 */
public class DemoTest {
    public static boolean demoOne(String str) {
        // 该正则表达式可以匹配所有的数字 包括负数
        Pattern pattern = Pattern.compile("-?[0-9]+(\\.[0-9]+)?");
        String bigStr;
        try {
            bigStr = new BigDecimal(str).toString();
        } catch (Exception e) {
            return false;//异常 说明包含非数字。
        }

        Matcher isNum = pattern.matcher(bigStr); // matcher是全匹配
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        System.out.println("Boolean: " + demoOne("222"));
    }
}
