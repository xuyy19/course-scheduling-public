package com.aliwo.stream.lambda;

import java.io.File;

/**
 * @author xuyayuan
 * @data 2021年05月20日
 */
public class DemoAmage {
    public static void main(String[] args) {
        isDirectory(new File("D:/test/src/"));
    }


    public static void isDirectory(File file) {
        if(file.exists()){
            if (file.getName().indexOf(".jpg") != -1) {
                System.out.println("file is ==>>" + file.getAbsolutePath());
            }else{
                File[] list = file.listFiles();//list获取的结果：
                if (list.length == 0) {
                    System.out.println(file.getAbsolutePath() + " is null");
                } else {
                    for (int i = 0; i < list.length; i++) {
                        if (list[i].getName().indexOf(".jpg") != -1){
                            isDirectory(list[i]);
                        }
                    }
                }
            }
        }else{
            System.out.println("文件不存在！");
        }
    }
}
