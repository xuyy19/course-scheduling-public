一:在电脑上下载node.js,配置vue脚手架 安装cnpm 命令具体安装教程可以百度搜索vue安装教程。
二:安装完vue所需要的运行时环境,通过wir+R输入cmd命令进入进入工程所在目录,
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/213424_2018c84a_7927376.png "屏幕截图.png")
2.2：进入UI目录然后进入coursearrange目录下。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/213548_12a4ace4_7927376.png "屏幕截图.png")
三:在coursearrange目录下输入 npm install 如果安装了cnpm(建议安装)命令输入 cnpm install
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/213644_d3217929_7927376.png "屏幕截图.png")
四:执行完 第三步命令后执行 npm run dev 命令 可以启动前端页面的加载效果了
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/213745_06ce8b2d_7927376.png "屏幕截图.png")
访问localhost:8081
看到此界面证明启动成功，开始探索吧！！！
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/213836_849d990a_7927376.png "屏幕截图.png")
上述如果启动有问题请添加QQ:1462638689
